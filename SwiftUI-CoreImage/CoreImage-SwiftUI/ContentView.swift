//
//  ContentView.swift
//  CoreImage-SwiftUI
//
//  Created by James Moore on 19/07/2019.
//  Copyright © 2019 James Moore. All rights reserved.
//

import SwiftUI

// NOTES: Pin this view in the Canvas so you can see image processing update when you changed values in ImageProcessing.swift.

// Show combined view with Original and Processed Images
struct ContentView : View {

    var body: some View {
        ZStack {
            VStack {
                // Show original version of image (you can update with your own in ImageProcessing)
                OriginalImageView()
                    .padding(.horizontal, 10)
                    .frame(width: 400, height: 300, alignment: .center)
                    .aspectRatio(ImageProcessing.sharedImage.inputImage!.size, contentMode: .fit)
                
                // Custom view showing the processed image
                ProcessedImageView()
                    .padding(.horizontal, 10)
                    .frame(width: 400, height: 300, alignment: .center)
                    .aspectRatio(UIImage(named: "flower")!.size, contentMode: .fit)
                
            }
        }
    }
    
}

// Original Image container
struct OriginalImageView : View {
    var body: some View {
        let img = UIImage(cgImage: ImageProcessing.sharedImage.inputImage!.cgImage!)
        return Image(uiImage: img).resizable()
    }
}

// Processed Image container
struct ProcessedImageView : View {
    var body: some View {
        let img = UIImage(cgImage: ImageProcessing.sharedImage.render(intensity: 1.0))
        return Image(uiImage: img).resizable()
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif

//
//  ImageProcessing.swift
//  CoreImage-SwiftUI
//
//  Created by James Moore on 19/07/2019.
//  Copyright © 2019 James Moore. All rights reserved.
//

import UIKit
import CoreImage

class ImageProcessing: NSObject {

    static let sharedImage = ImageProcessing() // Set up a shared instance to allow image data to be passed between classes
    let context = CIContext(options: nil) // Set up a single context to use for image processing
    let inputImage = UIImage(named: "flower.JPG")  // Update this with your own image
    
    // Render a processed image
    func render(intensity: Float) -> CGImage {
        let processedImage = sepiaFilter(inputImage!, intensity: 1.0) // CoreImage Sepia Filter - update INTENSITY to see changes live (0.0 - 1.0) for SEPIA effect
        let finalImage = completeProcessing(processedImage!) // Wrap image in CGContext and return as CGImage for next steps
        return finalImage.cgImage!
    }
    
    // CoreImage Sepia Filter
    func sepiaFilter(_ inputImage: UIImage, intensity: Double) -> CIImage? {
        var outputImage = CIImage()
        if let currentFilter = CIFilter(name: "CISepiaTone") {
            let beginImage = CIImage(image: inputImage)
            currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
            currentFilter.setValue(intensity, forKey: kCIInputIntensityKey)
            outputImage = currentFilter.outputImage!
        }
        return outputImage
    }
    
    // Wrap image in CGContext and return as CGImage for next steps
    func completeProcessing(_ inputImage: CIImage) -> UIImage {
        var outputImage = UIImage()
        let output = inputImage
        if let cgimg = context.createCGImage(output, from: output.extent) {
            outputImage = UIImage(cgImage: cgimg)
        }
        return outputImage
    }
}
